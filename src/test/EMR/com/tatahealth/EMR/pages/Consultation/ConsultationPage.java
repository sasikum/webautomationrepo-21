package com.tatahealth.EMR.pages.Consultation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ConsultationPage {
	
	
	
	public WebElement clickAllSlots(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='appointmentToggle']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement loadsAppointmentDashboard(WebDriver driver,ExtentTest logger)throws Exception{
		String xpath = "//a[@id='orgLogoLinkHeader']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}

	
	public WebElement getUserProfileName(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='userProfileName']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
}
