package com.tatahealth.EMR.pages.Consultation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class LabPeriodicityPages {
	
	public WebElement getLabTest(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("(//div[@class='topspacemore'])[3]", driver, logger);
 		
	}
	
	public WebElement getRepeatFollowUp(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//th[contains(text(),'Repeat')]", driver, logger);
 		
	}
	
	public WebElement getEveryTextfield(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='labFollowUpInput1']", driver, logger);	
	}
	public WebElement getEveryTextfield2(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='labFollowUpInput2']", driver, logger);	
	}
	public WebElement getEveryTextfield3(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='labFollowUpInput3']", driver, logger);	
	}
	
	public WebElement getForTextfield(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='labFollowUpexpiryAfter1']", driver, logger);	
	}
	public WebElement getForTextfield2(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='labFollowUpexpiryAfter2']", driver, logger);	
	}
	public WebElement getForTextfield3(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='labFollowUpexpiryAfter3']", driver, logger);	
	}
	public WebElement getNextTestOnText(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//span[contains(text(),'Next test on')]", driver, logger);	
	}
	
	public WebElement getEveryDropdown(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("(//select[@class='labtestdropdown'])[last()]", driver, logger);	
	}
	public WebElement getEveryDropdown2(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//select[@id='labFollowUpSelect2']", driver, logger);	
	}
	public WebElement getEveryDropdown3(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//select[@id='labFollowUpSelect3']", driver, logger);	
	}
	public WebElement getEveryDropdownLast(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("(//select[@class='labtestdropdown'])[last()]", driver, logger);	
	}
	public WebElement getForDropdown(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//select[@id='labFollowUpexpiryUnit1']", driver, logger);	
	}
	public WebElement getForDropdown2(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//select[@id='labFollowUpexpiryUnit2']", driver, logger);	
	}
	public WebElement getForDropdown3(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//select[@id='labFollowUpexpiryUnit3']", driver, logger);	
	}
	public WebElement getlabTestTamplateSearch(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='labTestsTemplateSearchField']", driver, logger);	
	}
	public WebElement getNotesTestOrScan(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("(//input[@name='notesTestOrScan'])[1]", driver, logger);	
	}
	public WebElement getSearchTestOrScan(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='testOrScanSearchString1']", driver, logger);	
	}
	public WebElement getCleanOptionBtn(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//button[@class='text-center btn btn-danger deleteTestOrScan']", driver, logger);	
	}
	public WebElement getCleanOption(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//span[@class='glyphicon glyphicon-remove']", driver, logger);	
	}
	public WebElement getToastMessage(WebDriver driver,ExtentTest logger) 
	{
		WebElement element = Login_Doctor.driver.findElement(By.className("toast-message"));
		return element;
		//return Web_GeneralFunctions.findElementbyXPath("//div[contains(text(),' Please fill mandatory field before saving')]", driver, logger);	
	}
	public WebElement getcalculatedDate(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//span[@id='labFollowUpDate1']", driver, logger);	
	}
	public WebElement getcalculatedDate2(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//span[@id='labFollowUpDate2']", driver, logger);	
	}
	public WebElement getPlusIcon(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//a[@class='btn takephotobtn allbtnfl']", driver, logger);	
	}
	public WebElement getSearchTestOrScan2(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='testOrScanSearchString2']", driver, logger);	
	}
	public WebElement getSearchTestOrScan3(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='testOrScanSearchString3']", driver, logger);	
	}
	public WebElement getSearchTestOrScan4(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='testOrScanSearchString4']", driver, logger);	
	}
	public WebElement getSearchTestOrScanLast(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("(//input[@name='testNameTestOrScan'])[last()]", driver, logger);	
	}
	public WebElement getNotesTestOrScan2(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='notesTestOrScan2']", driver, logger);	
	}
	public WebElement getNotesTestOrScan3(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='notesTestOrScan3']", driver, logger);	
	}
	public WebElement getNotesTestOrScan4(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//input[@id='notesTestOrScan4']", driver, logger);	
	}
	public WebElement getNotesTestOrScanLast(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("(//input[@name='notesTestOrScan'])[last()]", driver, logger);	
	}
	public WebElement getEMRHomeLink(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//a[@id='powredByLink']", driver, logger);	
	}
	public WebElement getConsulationCheckbox(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//span[@class='checkmarkfollowup']", driver, logger);	
	}
	
	public WebElement getRepeatFollowSection(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//a[@id='FollowUpPrintSelection']", driver, logger);	
	}
}
