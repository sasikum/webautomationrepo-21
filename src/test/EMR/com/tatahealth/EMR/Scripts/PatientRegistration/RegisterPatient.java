package com.tatahealth.EMR.Scripts.PatientRegistration;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.AllSlotsPage;
import com.tatahealth.EMR.pages.CommonPages.CommonPage;
import com.tatahealth.EMR.pages.RegisterPatient.PatientRegisterPage;
import com.tatahealth.EMR.pages.RegisterPatient.PatientRegisterPopup;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class RegisterPatient {
	
	
	@BeforeClass(groups= {"Regression","Appointment_Doctor"})
	public static void beforeClass() throws Exception {
		Login_Doctor.LoginTest();
		System.out.println("Initialized Driver");
	}
	
	@AfterClass(groups= {"Regression","Appointment_Doctor"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Driver");
	}
	
	
	ExtentTest logger;
	public static String mobileNo;
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * Moving to All Slots Page and Registering Patient using Global Plus Icon
	 * Also Checking certain Checks before 
	 */

	@Test(priority=101,groups= {"Regression","PatientRegistration"})
	public synchronized void PR_001() throws Exception {
			logger = Reports.extent.createTest("PR_001");
		//AppointmentsPage appoiPage = new AppointmentsPage();
			AllSlotsPage AsPage = new AllSlotsPage();
			CommonPage cpPage = new CommonPage();
			PatientRegisterPage ParPage = new PatientRegisterPage();
			Web_GeneralFunctions.click(AsPage.getPatientRegisterBtn(Login_Doctor.driver, logger), "Clicking on Patient Register Button", Login_Doctor.driver, logger);
			Thread.sleep(5000);
			
		//Now in Register Page. Not Putting any Details and saving
			Web_GeneralFunctions.click(ParPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking to Save without any Mandatory Details", Login_Doctor.driver, logger);
			Thread.sleep(1000);
		//capture popup
			Assert.assertEquals("Select a title", ParPage.getAlertText(Login_Doctor.driver, logger).trim(), "Proper Message is not Coming");
		
		
		//Now Adding title and Checking Alert Again
			Web_GeneralFunctions.click(ParPage.getTitleDropDown(Login_Doctor.driver, logger), "Clicking to Save without any Mandatory Details", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.setTitlefromDropdown(Login_Doctor.driver, logger), "Clicking to Save ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
			Thread.sleep(1000);
			Assert.assertEquals("Please fill the first name", ParPage.getAlertText(Login_Doctor.driver, logger).trim(), "Proper Message is not Coming");
		
		//Now Adding Name too and Checking Alert Again
			Web_GeneralFunctions.sendkeys(ParPage.getNameTextBox(Login_Doctor.driver, logger), RandomStringUtils.randomAlphabetic(10), "Clicking to Save without any Mandatory Details", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
			Thread.sleep(1000);
		
		
		
		//Now Adding Gender too and Checking Alert Again
			if(ParPage.getGenderDropDown(Login_Doctor.driver, logger).isEnabled()) {
				Assert.assertEquals("Please Select a Gender", ParPage.getAlertText(Login_Doctor.driver, logger).trim(), "Proper Message is not Coming");
				Web_GeneralFunctions.click(ParPage.getGenderDropDown(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
				Web_GeneralFunctions.click(ParPage.setGenderfromDropdown(Login_Doctor.driver, logger),"Clicking to Save", Login_Doctor.driver, logger);
				Web_GeneralFunctions.click(ParPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
			}
			
			Assert.assertEquals("Age is mandatory", ParPage.getAlertText(Login_Doctor.driver, logger).trim(), "Proper Message is not Coming");
			
		//Now Adding Age(Also Checking Age to DOB Conversion) and Checking
			Web_GeneralFunctions.sendkeys(ParPage.getAgeinYears(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(2), "", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.getAgeinMonths(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(1), "", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.getAgeinDays(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(1), "", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
			Thread.sleep(1000);
			Assert.assertEquals("Please Enter Your Mobile No", ParPage.getAlertText(Login_Doctor.driver, logger).trim(), "Proper Message is not Coming");
			System.out.println(ParPage.getDOBValue(Login_Doctor.driver, logger));
			
			
			
		//Adding Mobile Number and Saving
			mobileNo = "1212"+RandomStringUtils.randomNumeric(6);
			Web_GeneralFunctions.sendkeys(ParPage.setMobileNo(Login_Doctor.driver, logger), mobileNo, "", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
			String alert = cpPage.getPatientRegistrationToast(Login_Doctor.driver, logger).getText();
			System.out.println(alert);
			Assert.assertEquals("Patient", alert.substring(0, 7).trim(), "Proper Message is not Coming");
			
			Thread.sleep(5000);
	}
	
	
	
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * More and Other Checks
	 */
	@Test(priority=102,groups= {"Regression","PatientRegistration"})
	public synchronized void PR_002() throws Exception {
			logger = Reports.extent.createTest("PR_002");
			AllSlotsPage AsPage = new AllSlotsPage();
			PatientRegisterPage ParPage = new PatientRegisterPage();
			CommonPage cpPage = new CommonPage();
		
			Web_GeneralFunctions.click(AsPage.getPatientRegisterBtn(Login_Doctor.driver, logger), "Clicking on Patient Register Button", Login_Doctor.driver, logger);
			Thread.sleep(5000);
			
			
		//Now Adding title
			Web_GeneralFunctions.click(ParPage.getTitleDropDown(Login_Doctor.driver, logger), "Clicking to Save without any Mandatory Details", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.setTitlefromDropdown(Login_Doctor.driver, logger), "Clicking to Save ", Login_Doctor.driver, logger);
		
		//Now Adding Name
			Web_GeneralFunctions.sendkeys(ParPage.getNameTextBox(Login_Doctor.driver, logger), RandomStringUtils.randomAlphabetic(10), "Clicking to Save without any Mandatory Details", Login_Doctor.driver, logger);
		
		//Now Adding Gender
			if(ParPage.getGenderDropDown(Login_Doctor.driver, logger).isEnabled()) {
				Web_GeneralFunctions.click(ParPage.getGenderDropDown(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
				Web_GeneralFunctions.click(ParPage.setGenderfromDropdown(Login_Doctor.driver, logger),"Clicking to Save", Login_Doctor.driver, logger);
			}
		
		
		//Now Adding Age
			Web_GeneralFunctions.sendkeys(ParPage.getAgeinYears(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(2), "", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.getAgeinMonths(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(1), "", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.getAgeinDays(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(1), "", Login_Doctor.driver, logger);
			
			Thread.sleep(5000);
			
		
		
		//Adding Mobile Number
			Web_GeneralFunctions.click(ParPage.setMobileNoNA(Login_Doctor.driver, logger), "Clicking on N/A Button", Login_Doctor.driver, logger);
			
		//Adding improper email id and Saving
			Web_GeneralFunctions.sendkeys(ParPage.getEmailIdTextbox(Login_Doctor.driver, logger), "fjsfbskjeW!#!@#", "Clicking on N/A Button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
			Thread.sleep(1000);
			System.out.println(ParPage.getAlertText(Login_Doctor.driver, logger));
			//Assert.assertEquals("Please Enter Value", ParPage.getAlertText(Login_Doctor.driver, logger).trim(), "Proper Message is not Coming");
			
		//Adding Proper email
			Web_GeneralFunctions.clear(ParPage.getEmailIdTextbox(Login_Doctor.driver, logger), "Clicking on N/A Button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.getEmailIdTextbox(Login_Doctor.driver, logger), "dummy@dummy.dum", "Clicking on N/A Button", Login_Doctor.driver, logger);
			
		//Clicking on More
			Web_GeneralFunctions.click(ParPage.getMoreBtn(Login_Doctor.driver, logger), "Clicking on More Button", Login_Doctor.driver, logger);
		
		//Clicking on Religion Spinner
			Web_GeneralFunctions.click(ParPage.getReligionSelector(Login_Doctor.driver, logger), "Clicking to get religion dropdown", Login_Doctor.driver, logger);
		
		//Selecting other in Religion
			Web_GeneralFunctions.click(ParPage.getOtherAsReligion(Login_Doctor.driver, logger), "Clicking to set religion to others", Login_Doctor.driver, logger);
		
		//Saving without filling other religion details
			Web_GeneralFunctions.click(ParPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
			Thread.sleep(1000);
			Assert.assertEquals("Please Enter Value", ParPage.getAlertText(Login_Doctor.driver, logger).trim(), "Proper Message is not Coming");
		
		//Adding Other Relegion and Selecting Govt Id type
			Thread.sleep(2000);
			Web_GeneralFunctions.sendkeys(ParPage.setOtherReligion(Login_Doctor.driver, logger), RandomStringUtils.randomAlphabetic(6), "Some dummy Religion", Login_Doctor.driver, logger);
			Web_GeneralFunctions.scrollElementToCenter(ParPage.getGovtIdTypeSelector(Login_Doctor.driver, logger), Login_Doctor.driver);
			Web_GeneralFunctions.click(ParPage.getGovtIdTypeSelector(Login_Doctor.driver, logger), "Clicking on More Button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.setRandomGovtIdType(Login_Doctor.driver, logger), "Clicking on More Button", Login_Doctor.driver, logger);
		
		//Saving without filling Govt Id No
			Web_GeneralFunctions.click(ParPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
			Thread.sleep(1000);
			Assert.assertEquals("Please fill the government id number", ParPage.getAlertText(Login_Doctor.driver, logger).trim(), "Proper Message is not Coming");
		
		//Setting Govt Id number and MLC yes but no number or Description
			Web_GeneralFunctions.sendkeys(ParPage.setGovtIdNumber(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(6), "Some dummy Religion", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.getMlcYesRadioBtn(Login_Doctor.driver, logger), "Clicking to Save MLC Yes", Login_Doctor.driver, logger);
		
		//Saving without filling MLC Details
			Web_GeneralFunctions.click(ParPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
			Thread.sleep(1000);
			Assert.assertEquals("Please Enter Value", ParPage.getAlertText(Login_Doctor.driver, logger).trim(), "Proper Message is not Coming");
		
		//Adding MLC detils and saving
			Web_GeneralFunctions.sendkeys(ParPage.getMlcNumberTextbox(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(6), "Some dummy MLC Number", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.getMlcDescTextBox(Login_Doctor.driver, logger), RandomStringUtils.randomAlphabetic(6), "Some dummy MLC Description", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
			String alert = cpPage.getPatientRegistrationToast(Login_Doctor.driver, logger).getText();
			System.out.println(alert);
			Assert.assertEquals("Patient", alert.substring(0, 7).trim(), "Proper Message is not Coming");
			Thread.sleep(5000);
	}
	
	

	@Test(priority=103,groups= {"Regression","PatientRegistration"})
	public synchronized void PR_003() throws Exception {
			logger = Reports.extent.createTest("PR_003");
		//AppointmentsPage appoiPage = new AppointmentsPage();
			AllSlotsPage AsPage = new AllSlotsPage();
			CommonPage cpPage = new CommonPage();
			PatientRegisterPage ParPage = new PatientRegisterPage();
			PatientRegisterPopup popupPage = new PatientRegisterPopup();
			Web_GeneralFunctions.click(AsPage.getPatientRegisterBtn(Login_Doctor.driver, logger), "Clicking on Patient Register Button", Login_Doctor.driver, logger);
			Thread.sleep(5000);
		
		
		//Now Adding title
			Web_GeneralFunctions.click(ParPage.getTitleDropDown(Login_Doctor.driver, logger), "Clicking to Save without any Mandatory Details", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.setTitlefromDropdown(Login_Doctor.driver, logger), "Clicking to Save ", Login_Doctor.driver, logger);
			
		
		//Now Adding Name too and Checking Alert Again
			Web_GeneralFunctions.sendkeys(ParPage.getNameTextBox(Login_Doctor.driver, logger), RandomStringUtils.randomAlphabetic(10), "Clicking to Save without any Mandatory Details", Login_Doctor.driver, logger);
		
		
		//Now Adding Gender too and Checking Alert Again
			if(ParPage.getGenderDropDown(Login_Doctor.driver, logger).isEnabled()) {
				Web_GeneralFunctions.click(ParPage.getGenderDropDown(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
				Web_GeneralFunctions.click(ParPage.setGenderfromDropdown(Login_Doctor.driver, logger),"Clicking to Save", Login_Doctor.driver, logger);
				Web_GeneralFunctions.click(ParPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
			}
			
		//Now Adding Age(Also Checking Age to DOB Conversion) and Checking
			Web_GeneralFunctions.sendkeys(ParPage.getAgeinYears(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(2), "", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.getAgeinMonths(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(1), "", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.getAgeinDays(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(1), "", Login_Doctor.driver, logger);
			
			
			
			
		//Adding Mobile Number and Saving
			//mobileNo = "1212"+RandomStringUtils.randomNumeric(6);
			Web_GeneralFunctions.sendkeys(ParPage.setMobileNo(Login_Doctor.driver, logger), mobileNo, "", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
			
		
		//Saving with Existing Patient
			Web_GeneralFunctions.click(popupPage.getExistingPatientSelection(Login_Doctor.driver, logger), "Clicking to save as Existing Patient", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(popupPage.getConfirmBtn(Login_Doctor.driver, logger), "Clicking to Confirm", Login_Doctor.driver, logger);
			
			String alert = cpPage.getPatientRegistrationToast(Login_Doctor.driver, logger).getText();
			System.out.println(alert);
			//Assert.assertEquals("Patient", alert.substring(0, 7).trim(), "Proper Message is not Coming");
			
			Thread.sleep(5000);
	}
	
	
	
	
	

	@Test(priority=104,groups= {"Regression","PatientRegistration"})
	public synchronized void PR_004() throws Exception {
			logger = Reports.extent.createTest("PR_003");
		//AppointmentsPage appoiPage = new AppointmentsPage();
			AllSlotsPage AsPage = new AllSlotsPage();
			CommonPage cpPage = new CommonPage();
			PatientRegisterPage ParPage = new PatientRegisterPage();
			PatientRegisterPopup popupPage = new PatientRegisterPopup();
			Web_GeneralFunctions.click(AsPage.getPatientRegisterBtn(Login_Doctor.driver, logger), "Clicking on Patient Register Button", Login_Doctor.driver, logger);
			Thread.sleep(5000);
		
		
		//Now Adding title
			Web_GeneralFunctions.click(ParPage.getTitleDropDown(Login_Doctor.driver, logger), "Clicking to Save without any Mandatory Details", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.setTitlefromDropdown(Login_Doctor.driver, logger), "Clicking to Save ", Login_Doctor.driver, logger);
			
		
		//Now Adding Name too and Checking Alert Again
			Web_GeneralFunctions.sendkeys(ParPage.getNameTextBox(Login_Doctor.driver, logger), RandomStringUtils.randomAlphabetic(10), "Clicking to Save without any Mandatory Details", Login_Doctor.driver, logger);
		
		
		//Now Adding Gender too and Checking Alert Again
			if(ParPage.getGenderDropDown(Login_Doctor.driver, logger).isEnabled()) {
				Web_GeneralFunctions.click(ParPage.getGenderDropDown(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
				Web_GeneralFunctions.click(ParPage.setGenderfromDropdown(Login_Doctor.driver, logger),"Clicking to Save", Login_Doctor.driver, logger);
				Web_GeneralFunctions.click(ParPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
			}
			
		//Now Adding Age(Also Checking Age to DOB Conversion) and Checking
			Web_GeneralFunctions.sendkeys(ParPage.getAgeinYears(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(2), "", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.getAgeinMonths(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(1), "", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.getAgeinDays(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(1), "", Login_Doctor.driver, logger);
			
			
			
			
		//Adding Mobile Number and Saving
			//mobileNo = "1212"+RandomStringUtils.randomNumeric(6);
			Web_GeneralFunctions.sendkeys(ParPage.setMobileNo(Login_Doctor.driver, logger), mobileNo, "", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
			
		
		//Saving with Existing Patient
			Web_GeneralFunctions.click(popupPage.getNewPatientSelection(Login_Doctor.driver, logger), "Clicking to save as New Patient", Login_Doctor.driver, logger);
			
			String alert = cpPage.getPatientRegistrationToast(Login_Doctor.driver, logger).getText();
			System.out.println(alert);
			//Assert.assertEquals("Patient", alert.substring(0, 7).trim(), "Proper Message is not Coming");
			
			Thread.sleep(5000);
	}
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * More and Other Checks
	 */

	@Test(priority=105,groups= {"Regression","PatientRegistration"})
	public synchronized void PR_005() throws Exception {
			logger = Reports.extent.createTest("PR_002");
			AllSlotsPage AsPage = new AllSlotsPage();
			PatientRegisterPage ParPage = new PatientRegisterPage();
			
			Web_GeneralFunctions.click(AsPage.getPatientRegisterBtn(Login_Doctor.driver, logger), "Clicking on Patient Register Button", Login_Doctor.driver, logger);
			Thread.sleep(5000);
			
			
		//Now Adding title
			Web_GeneralFunctions.click(ParPage.getTitleDropDown(Login_Doctor.driver, logger), "Clicking to Save without any Mandatory Details", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.setTitlefromDropdown(Login_Doctor.driver, logger), "Clicking to Save ", Login_Doctor.driver, logger);
		
		//Now Adding Name
			Web_GeneralFunctions.sendkeys(ParPage.getNameTextBox(Login_Doctor.driver, logger), RandomStringUtils.randomAlphabetic(10), "Clicking to Save without any Mandatory Details", Login_Doctor.driver, logger);
		
		//Now Adding Gender
			if(ParPage.getGenderDropDown(Login_Doctor.driver, logger).isEnabled()) {
				Web_GeneralFunctions.click(ParPage.getGenderDropDown(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
				Web_GeneralFunctions.click(ParPage.setGenderfromDropdown(Login_Doctor.driver, logger),"Clicking to Save", Login_Doctor.driver, logger);
			}
		
		
		//Now Adding Age
			Web_GeneralFunctions.sendkeys(ParPage.getAgeinYears(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(2), "Adding years", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.getAgeinMonths(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(1), "Adding months", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.getAgeinDays(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(1), "Adding days", Login_Doctor.driver, logger);
			
			
		
		
		//Adding Mobile Number
			Web_GeneralFunctions.click(ParPage.setMobileNoNA(Login_Doctor.driver, logger), "Clicking on N/A Button", Login_Doctor.driver, logger);
			
			
			
		//Selecting and deselecting Nil without allergies
			Web_GeneralFunctions.click(ParPage.setAllergiesNil(Login_Doctor.driver, logger), "selecting Allergies Nil", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.setAllergiesNil(Login_Doctor.driver, logger), "unselecting Allergies Nil", Login_Doctor.driver, logger);
			
			
		//Adding Allergies
			Web_GeneralFunctions.click(ParPage.getAllergies(Login_Doctor.driver, logger), "Clicking on allergies textbox", Login_Doctor.driver, logger);
			Thread.sleep(2000);
			Web_GeneralFunctions.sendkeys(ParPage.setAllergies(Login_Doctor.driver, logger), "Allergy1"+Keys.ENTER, "Adding data in allergies", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.setAllergies(Login_Doctor.driver, logger), "Allergy2"+Keys.ENTER, "Adding data in allergies", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.setAllergiesNil(Login_Doctor.driver, logger), "selecting Allergies Nil", Login_Doctor.driver, logger);
			Thread.sleep(2000);
			Web_GeneralFunctions.click(ParPage.sweetAlertConfirm(Login_Doctor.driver, logger), "selecting Allergies Nil", Login_Doctor.driver, logger);
			Thread.sleep(3000);
			Assert.assertEquals(true, ParPage.setAllergiesNil(Login_Doctor.driver, logger).isSelected());
			
			Web_GeneralFunctions.click(ParPage.setAllergiesNil(Login_Doctor.driver, logger), "unselecting Allergies Nil", Login_Doctor.driver, logger);
			
			Web_GeneralFunctions.click(ParPage.getAllergies(Login_Doctor.driver, logger), "Clicking on allergies textbox", Login_Doctor.driver, logger);
			Thread.sleep(2000);
			Web_GeneralFunctions.sendkeys(ParPage.setAllergies(Login_Doctor.driver, logger), "Allergy1"+Keys.ENTER, "Adding data in allergies", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.setAllergies(Login_Doctor.driver, logger), "Allergy2"+Keys.ENTER, "Adding data in allergies", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.setAllergiesNil(Login_Doctor.driver, logger), "selecting Allergies Nil", Login_Doctor.driver, logger);
			Thread.sleep(2000);
			Web_GeneralFunctions.click(ParPage.sweetAlertCancel(Login_Doctor.driver, logger), "selecting Allergies Nil", Login_Doctor.driver, logger);
			Assert.assertEquals(false, ParPage.setAllergiesNil(Login_Doctor.driver, logger).isSelected());
			
		//Adding Pollen
			Web_GeneralFunctions.click(ParPage.getAllergies(Login_Doctor.driver, logger), "Clicking on allergies textbox", Login_Doctor.driver, logger);
			Thread.sleep(2000);
			Web_GeneralFunctions.sendkeys(ParPage.setAllergies(Login_Doctor.driver, logger), "Pollen", "Adding data in allergies", Login_Doctor.driver, logger);
			Thread.sleep(3000);
			Web_GeneralFunctions.sendkeys(ParPage.setAllergies(Login_Doctor.driver, logger), Keys.ARROW_DOWN, "Adding data in allergies", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.setAllergies(Login_Doctor.driver, logger), Keys.ENTER, "Adding data in allergies", Login_Doctor.driver, logger);
			
			
		//Add new Social Habits Row check with empty row 
			Web_GeneralFunctions.click(ParPage.addSocialHabitsRow(0, Login_Doctor.driver, logger), "Clicking on Social Habits add Row Button", Login_Doctor.driver, logger);
			String alert = ParPage.getAlertText(Login_Doctor.driver, logger).trim();
			System.out.println(alert);
			
			Web_GeneralFunctions.sendkeys(ParPage.getSocialHabits(0, Login_Doctor.driver, logger), RandomStringUtils.randomAlphabetic(6), "Clicking on Social Habits textbox", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.getSocialHabitsDuration(0, Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(2), "Sending random numeric value to textbox", Login_Doctor.driver, logger);
			Assert.assertEquals(true, ParPage.getSocialHabitsDurationType(0, 2, Login_Doctor.driver, logger).isSelected());
			
			Web_GeneralFunctions.click(ParPage.getSocialHabitsDurationType(0, 1, Login_Doctor.driver, logger), "Clicking on Social Habits add Row Button", Login_Doctor.driver, logger);
			Assert.assertEquals(true, ParPage.getSocialHabitsDurationType(0, 1, Login_Doctor.driver, logger).isSelected());
			
			
			Web_GeneralFunctions.sendkeys(ParPage.getSocialHabitsComments(0, Login_Doctor.driver, logger), RandomStringUtils.randomAlphabetic(6), "Sending random numeric value to textbox", Login_Doctor.driver, logger);
			
			Web_GeneralFunctions.click(ParPage.addSocialHabitsRow(0, Login_Doctor.driver, logger), "Clicking on Social Habits add Row Button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ParPage.deleteSocialHabitsRow(0, Login_Doctor.driver, logger), "Clicking on Social Habits add Row Button", Login_Doctor.driver, logger);
			
			Web_GeneralFunctions.scrollElementToCenter(ParPage.setSocialHabitsNil(Login_Doctor.driver, logger), Login_Doctor.driver);
			Web_GeneralFunctions.click(ParPage.setSocialHabitsNil(Login_Doctor.driver, logger), "Clicking on Social Habits add Row Button", Login_Doctor.driver, logger);
			Assert.assertEquals(true, ParPage.setSocialHabitsNil(Login_Doctor.driver, logger).isSelected());
			
			Web_GeneralFunctions.sendkeys(ParPage.getSocialHabits(0, Login_Doctor.driver, logger), RandomStringUtils.randomAlphabetic(6), "Clicking on Social Habits textbox", Login_Doctor.driver, logger);
			
			Web_GeneralFunctions.click(ParPage.setSocialHabitsNil(Login_Doctor.driver, logger), "Clicking on Social Habits add Row Button", Login_Doctor.driver, logger);
			
			Thread.sleep(2000);
			Web_GeneralFunctions.click(ParPage.sweetAlertConfirm(Login_Doctor.driver, logger), "selecting Allergies Nil", Login_Doctor.driver, logger);
			Assert.assertEquals(true, ParPage.setSocialHabitsNil(Login_Doctor.driver, logger).isSelected());
			
			//String randSocHabit = RandomStringUtils.randomAlphabetic(6);
			//Web_GeneralFunctions.sendkeys(ParPage.getSocialHabits(0, Login_Doctor.driver, logger), randSocHabit, "Clicking on Social Habits textbox", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.getSocialHabitsDuration(0, Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(2), "Sending random numeric value to textbox", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(ParPage.getSocialHabitsComments(0, Login_Doctor.driver, logger), RandomStringUtils.randomAlphabetic(6), "Sending random numeric value to textbox", Login_Doctor.driver, logger);
			
			Web_GeneralFunctions.click(ParPage.setSocialHabitsNil(Login_Doctor.driver, logger), "Clicking on Social Habits add Row Button", Login_Doctor.driver, logger);
			
			Thread.sleep(2000);
			Web_GeneralFunctions.click(ParPage.sweetAlertCancel(Login_Doctor.driver, logger), "selecting Allergies Nil", Login_Doctor.driver, logger);
			Assert.assertEquals(false, ParPage.setSocialHabitsNil(Login_Doctor.driver, logger).isSelected());
			
			
	}
}