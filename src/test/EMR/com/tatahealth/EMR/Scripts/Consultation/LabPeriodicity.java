package com.tatahealth.EMR.Scripts.Consultation;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.LabPeriodicityPages;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class LabPeriodicity {
	public static LabPeriodicityPages lab = new LabPeriodicityPages();
	public static ConsultationTest consult = new ConsultationTest();
	public static ExtentTest logger;
	public static String patientName = "Maha";
	public static Integer availableSlot = 0;
	public static String prevSlotId = "";
	public static String slotId = "";
	
	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		Reports.reports();
		Login_Doctor.executionName = "Lab Test Periodicity";
		Login_Doctor.LoginTestwithDiffrentUser("Kasu");
		consult.labStartConsultation("Maha");
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(lab.getLabTest(Login_Doctor.driver, logger), "Scroll page till Lab test is visible on the page", 
				Login_Doctor.driver, logger);
	}
	@AfterClass(alwaysRun = true)
	public static void afterClass() throws Exception {
		Web_GeneralFunctions.scrollDown(Login_Doctor.driver);
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}	
	@Test(priority = 1, groups = { "Regression", "LabPeriodicity" }, description = "LTP_01")
	public void Lab_Tests() throws Exception {
		logger = Reports.extent.createTest("To verify Lab Tests is present in Consultation page");
		Assert.assertTrue(lab.getLabTest(Login_Doctor.driver, logger).isDisplayed(), "Lab Test is present in the consultation screen");
	}
	@Test(priority = 2, groups = { "Regression", "LabPeriodicity" }, description = "LTP_02")
	public void repeat_Follow_Up() throws Exception {
		logger = Reports.extent.createTest("To verify Repeat Follow Up column under Lab Tests");
		Assert.assertTrue(lab.getRepeatFollowUp(Login_Doctor.driver, logger).isDisplayed(), "Repeat Follow Up column is included for the Test name under Lab tests");

	}
	@Test(priority = 4, groups = { "Regression", "LabPeriodicity" }, description = "LTP_03, LTP_04")
	public void repeat_Follow_Up_CFC_clinics() throws Exception {
		logger = Reports.extent.createTest("To verify Repeat Follow up is present for CFC clinics");
		JavascriptExecutor js = (JavascriptExecutor)Login_Doctor.driver;
		String labTamplateText = "EMR Automation Testing";
		lab.getlabTestTamplateSearch(Login_Doctor.driver, logger).sendKeys(labTamplateText);
		js.executeScript("arguments[0].value='Urine Analysis';", lab.getSearchTestOrScan(Login_Doctor.driver, logger));
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(lab.getRepeatFollowUp(Login_Doctor.driver, logger).isDisplayed(), "Repeat Follow Up column is displayed");
		Web_GeneralFunctions.wait(3);
		js.executeScript("arguments[0].value='Creatinine';", lab.getSearchTestOrScan(Login_Doctor.driver, logger));
		Assert.assertTrue(lab.getRepeatFollowUp(Login_Doctor.driver, logger).isDisplayed(), "Repeat Follow Up column is displayed");
	}
	@Test(priority = 5, groups = { "Regression", "LabPeriodicity" }, description = "LTP_05, LTP_06, LTP_07")
	public void every_And_For_Textfileds() throws Exception {
		logger = Reports.extent.createTest("To verify Every(repeat period) and For(expiry period) textfileds in the Repeat Follow Up coulmn");
		Assert.assertTrue(lab.getEveryTextfield(Login_Doctor.driver, logger).isDisplayed(), "Every textfields is displayed in the Repeat Follow Up column");
		Assert.assertTrue(lab.getForTextfield(Login_Doctor.driver, logger).isDisplayed(), "For textfields is displayed in the Repeat Follow Up column");	
		Assert.assertTrue(lab.getNextTestOnText(Login_Doctor.driver, logger).isDisplayed(), " 'Next test on' text is displayed in the Repeat Follow Up column");
		Assert.assertTrue(lab.getEveryDropdown(Login_Doctor.driver, logger).isDisplayed(), "Every textfield contains two sub fields one is number textfield and another dropdown");	
	}	
	@Test(priority = 6, groups = { "Regression", "LabPeriodicity" }, description = "LTP_08, LTP_09, LTP_10, LTP_11")
	public void every_Textfileds_And_Dropdwon() throws Exception {
		logger = Reports.extent.createTest("To verify user can able to enter numbers in the textfield and and can select options from dropdwon");
		String expectedNumberEntered = RandomStringUtils.randomNumeric(1);
		lab.getEveryTextfield(Login_Doctor.driver, logger).sendKeys(expectedNumberEntered);
		Web_GeneralFunctions.wait(2);
		Select textDropdown = new Select(lab.getEveryDropdown(Login_Doctor.driver, logger));
		List<WebElement> dd = textDropdown.getOptions();
		    for(WebElement option : dd){
		    	option.click(); 
		    	Web_GeneralFunctions.wait(3);
		    	if (option.getAttribute("value")=="Day(s)") {
					Assert.assertTrue(true, "Day(s) selected");
				}
		    	else if (option.getAttribute("value")=="Week(s)") {
					Assert.assertTrue(true, "Week(s) selected");
				}
		    	else {
		    		Assert.assertTrue(true, "Month(s) selected");
		    	}
		    }
		String actualNumberEntered = lab.getEveryTextfield(Login_Doctor.driver, logger).getAttribute("value");
		try {
			Assert.assertEquals(actualNumberEntered, expectedNumberEntered, "User can to enter the numbers in the Every textfield");
		} catch (Exception e) {
			Assert.assertTrue(true, "User can to enter the numbers in the Every textfield");
		}	
	}
	@Test(priority = 7, groups = { "Regression", "LabPeriodicity" }, description = "LTP_12, LTP_13")
	public void for_Textfileds_And_Dropdwon() throws Exception {
		logger = Reports.extent.createTest("To verify user can able to enter numbers in the textfield and and can select options from dropdwon");
		String expectedNumber = RandomStringUtils.randomNumeric(1);		
		Assert.assertTrue(lab.getForTextfield(Login_Doctor.driver, logger).isDisplayed(), "For textfield contains number textfield sub fields ");
		Assert.assertTrue(lab.getForDropdown(Login_Doctor.driver, logger).isDisplayed(), "For textfield contains dropdown sub fields");		
		Web_GeneralFunctions.wait(2);
		lab.getForTextfield(Login_Doctor.driver, logger).sendKeys(expectedNumber);		
		String actualNumber = lab.getForTextfield(Login_Doctor.driver, logger).getAttribute("value");		
		Assert.assertEquals(actualNumber, expectedNumber);	
		lab.getForDropdown(Login_Doctor.driver, logger).click();
		Select textDropdown = new Select(lab.getForDropdown(Login_Doctor.driver, logger));
		 List<WebElement> dd = textDropdown.getOptions();		 
		 for (WebElement option : dd) {
			option.click();
			Web_GeneralFunctions.wait(3);
			if (option.getAttribute("value")=="Day(s)") {
				Assert.assertTrue(true, "Day(s) selected");
			}
	    	else if (option.getAttribute("value")=="Week(s)") {
				Assert.assertTrue(true, "Week(s) selected");
			}
	    	else {
	    		Assert.assertTrue(true, "Month(s) selected");
	    	}
		}	 
	}	
}