package com.tatahealth.EMR.Scripts.Billing;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ConsumerPackageTest {

	ConsultationTest appt=new ConsultationTest();
	BillingTest bs=new BillingTest();
	Billing bill = new Billing();
	ConsultationPage consultation = new ConsultationPage();
	ConsumerAppointmentTest consumer= new ConsumerAppointmentTest();
	public static ExtentTest logger;
	
	
	@BeforeClass(alwaysRun = true) public static void beforeClass() throws Exception { 
		  Reports.reports();
		  Login_Doctor.executionName="ConsumerPackageTest";
		  BillingTest.doctorName="Baba";
		  Login_Doctor.LoginTestwithDiffrentUser(BillingTest.doctorName);
		  }
	  
	  @AfterClass(alwaysRun = true) public static void afterClass() throws Exception {
		  Login_Doctor.LogoutTest();
			Reports.extent.flush(); 
		  }
	
	/*Billing_C_PP_2-pay at clinic is enabled from doctor side*/
	/*  Mention test case covered in all test below :RGB_5 ,VB_1-VB_24,Biiling_C_P_27-Biiling_C_P_33,Biiling_C_P_18,Biiling_C_P_23-Biiling_C_P_26
	 * Biiling_C_P_47-Biiling_C_P_48
	 */
	  
	  /*Test case:Billing_C_PP_12*/
		@Test(priority=87,groups = {"G3"})
		public void consumerPPaymentCashWalletEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Package Payment Emr Validation with Cash Wallet");
			String paymentCheck="Cash, Wallet, Package";
			String paidStatus="Cash, Wallet";
			consumer.consumerBookAppointment("Package");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModePStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerPBillingPageValidation();
			 bs.paymentCashWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashWalletPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModePStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashWalletOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashWalletPdfValidation();
		 
		}
		@Test(priority=88,groups = {"G2"})
		public void consumerPPaymentCashDebitCardEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Package Payment Emr Validation with Cash  Debit Card");
			String paymentCheck="Cash, Debit card, Package";
			String paidStatus="Cash, Debit card";
			consumer.consumerBookAppointment("Package");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModePStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerPBillingPageValidation();
			 bs.paymentCashDebitCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModePStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashDebitCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashCardPdfValidation();
		 
		}
		@Test(priority=89,groups = {"G1"})
		public void consumerPPaymentCashCreditCardEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Package Payment Emr Validation with Cash  Credit Card");
			String paymentCheck="Cash, Credit card, Package";
			String paidStatus="Cash, Credit card";
			consumer.consumerBookAppointment("Package");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModePStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerPBillingPageValidation();
			 bs.paymentCashCreditCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModePStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCashCreditCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashCardPdfValidation();
		 
		}
		@Test(priority=90,groups = {"G2"})
		public void consumerPPaymentCashEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Package Payment Emr Validation with Cash");
			String paymentCheck="Cash, Package";
			String paidStatus="Cash";
			consumer.consumerBookAppointment("Package");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModePStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerPBillingPageValidation();
			 bs.paymentCash();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCashPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModePStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCashOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCash();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCashPdfValidation();
		 
		}
		@Test(priority=91,groups = {"G3"})
		public void consumerPPaymentCreditCardEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Package Payment Emr Validation with Credit Card");
			String paymentCheck="Credit card, Package";
			String paidStatus="Credit card";
			consumer.consumerBookAppointment("Package");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModePStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerPBillingPageValidation();
			 bs.paymentCreditCard();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingCardPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModePStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedCardOption();
		  bs. billingCreatePaymentOptionDisplay();
		  bs.paymentCreditCard();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingCardPdfValidation();
		 
		}
		@Test(priority=92,groups = {"G1"})
		public void consumerPPaymentWalletEmrValidation()throws Exception
		{
			logger = Reports.extent.createTest("consumer Package Payment Emr Validation with Wallet ");
			String paymentCheck="Wallet, Package";
			String paidStatus="Wallet";
			consumer.consumerBookAppointment("Package");
			bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.appointmentPaymentModePStatusBooked();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
			 consumer.consumerPBillingPageValidation();
			 bs.paymentWallet();
			 bs.billingCreateBillPopUpValidation();
			 bs.billingWalletPdfValidation();
			 Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.appointmentPaymentModePStatusPaid(paidStatus);
		  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
		  bs.paymentRemoveSelectedWalletOption();
		  bs.billingCreatePaymentOptionDisplay();
		  bs.paymentWallet();
		  bs. billingCreateBillPopUpValidation();
		  bs.billingWalletPdfValidation();
		 
		}
}
