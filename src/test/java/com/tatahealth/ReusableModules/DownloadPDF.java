package com.tatahealth.ReusableModules;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.io.RandomAccessRead;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class DownloadPDF {


	 public String pdfDownload(String urls) throws Exception {
		 Web_GeneralFunctions.wait(1);
	  URL url = verify(urls);
	  
	  String fileCreateion = null;
	  int indexOfString = urls.indexOf(".pdf");
	  fileCreateion = urls.substring(0, indexOfString+4);
	  
	  URL u = verify(fileCreateion);

	  HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	  InputStream inputStream = null;
	  String filename = u.getFile();
	  filename = filename.substring(filename.lastIndexOf('/')+1);
	 // System.out.println("File Name : "+filename);
	  FileOutputStream outputStream = new FileOutputStream(filename);
	  inputStream = connection.getInputStream();

	  int read = -1;
	  byte[] buffer = new byte[4096]; 

	  while((read = inputStream.read(buffer))!= -1){
	   outputStream.write(buffer,0,read);

	  }
	  inputStream.close();
	  outputStream.close();
	  return filename;
	 }

	 private static URL verify(String url){ 
		// System.out.println("pdf url : "+url);
	  if(!url.toLowerCase().startsWith("https://")){
	   return null;
	  }
	  URL verifyURL= null;

	  try{
	   verifyURL = new URL(url);

	  }catch(Exception e){

	  }
	  return verifyURL;
	 }
	 
	 public String verifyPDFContent(String strURL) throws Exception {
			
			boolean flag = false;
			
			PDFTextStripper pdfStripper = null;
			PDDocument pdDoc = null;
			COSDocument cosDoc = null;
			String parsedText = null;

			try {
			/*	URL url = new URL(strURL);
				BufferedInputStream file = new BufferedInputStream(url.openStream());
				PDFParser parser = new PDFParser(file);*/
			
				
				strURL = "./"+strURL;
			//	PDFParser parser = new PDFParser((RandomAccessRead) new FileInputStream(new File(strURL)));
				PDFParser parser = new PDFParser(new RandomAccessFile(new File(strURL),"r"));
				parser.parse();
				cosDoc = parser.getDocument();
				pdfStripper = new PDFTextStripper();
				pdfStripper.setStartPage(1);
				
				
				pdDoc = new PDDocument(cosDoc);
				pdfStripper.setEndPage(pdDoc.getNumberOfPages());
				//System.out.println("no of pages : "+pdDoc.getNumberOfPages());
				parsedText = pdfStripper.getText(pdDoc);
				deleteFile(strURL);
			} catch (MalformedURLException e2) {
				System.err.println("URL string could not be parsed "+e2.getMessage());
			} catch (IOException e) {
				System.err.println("Unable to open PDF Parser. " + e.getMessage());
				try {
					if (cosDoc != null)
						cosDoc.close();
					if (pdDoc != null)
						pdDoc.close();
				} catch (Exception e1) {
					e.printStackTrace();
				}
			}
			
			if (cosDoc != null)
				cosDoc.close();
			if (pdDoc != null)
				pdDoc.close();
			return parsedText;
		}
	 
	 
	 public static void deleteFile(String file)throws Exception{
		 File deletefile = new File(file);
		 deletefile.delete();
		 
	 }
	
}